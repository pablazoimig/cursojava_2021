package modulo4;

import java.util.Scanner;

public class Ejercicio13
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el n�mero del mes");
		int mes = scan.nextInt();
		
		switch (mes)
		{
		case 2: 
			System.out.println("El mes ingresado tiene 28 dias");
			break;
		case 4: case 6: case 9: case 11:
			System.out.println("El mes ingresado tiene 30 dias");
			break;
		case 1: case 3: case 5: case 7: case 8: case 12:
			System.out.println("El mes ingresado tiene 31 dias");
			break;
		default:
			System.out.println("El n�mero de mes ingresado no es valido, pruebe con otro");
			break;
		}
		scan=null;
	}
}
