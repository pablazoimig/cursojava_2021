package modulo4;

import java.util.Scanner;

public class Ejercicio16
{
	public static void main(String[] args)
	{
		System.out.println("Ingrese el n�mero del que se quiera saber la tabla de multiplicar");
		Scanner scan = new Scanner(System.in);
		int numero = scan.nextInt();
		
		System.out.println("La siguiente tabla corresponde al n�mero:  " + numero);
		
		for(int i=0; i<11; i++ )
		{
			int resultado = numero * i;
			System.out.println(numero + "x" + i + "=" + resultado);
		}
		scan=null;
	}
}