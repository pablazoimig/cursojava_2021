package modulo4;

import java.util.Scanner;

public class Ejercicio10
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la Primera Variable");
		int PrimerValor =scan.nextInt();
		System.out.println("Ingrese la Segunda Variable");
		int SegundoValor =scan.nextInt();
		System.out.println("Ingrese la Tercera Variable");
		int TercerValor =scan.nextInt();
		
		if (PrimerValor > SegundoValor && SegundoValor > TercerValor)
			System.out.println("La Primer Variable " + PrimerValor + " es la Mayor");
		else if (SegundoValor > PrimerValor && SegundoValor > TercerValor)
			System.out.println("La Segunda Variable " + SegundoValor + " es la Mayor");
		else if (TercerValor > PrimerValor && TercerValor > SegundoValor)
			System.out.println("La Tercera Variable " + TercerValor + " es la Mayor");
		else
			System.out.println("Ning�n n�mero es mayor que otro");

		scan=null;
	}
}
