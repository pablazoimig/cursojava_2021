package modulo4;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un mes (Empiece con may�scula)");
		String grupo = scan.nextLine();
		
		if (grupo.equals("Abril") || grupo.equals("Junio") || grupo.equals("Septiembre") || grupo.equals("Noviembre"))
			System.out.println("Este mes tiene 30 dias");
		else if (grupo.equals("Enero") || grupo.equals("Marzo") || grupo.equals("Julio") || grupo.equals("Agosto") || grupo.equals("Octubre") || grupo.equals("Diciembre"))
			System.out.println("Este mes tiene 31 dias");
		else if (grupo.equals("Febrero"))
			System.out.println("Este mes tiene 28 dias");
		else
			System.out.println("El mes que ingres� no existe o es incorrecto");
		
		scan=null;
	}
}
