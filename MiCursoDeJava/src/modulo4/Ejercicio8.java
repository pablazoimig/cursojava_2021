package modulo4;

import java.util.Scanner;

public class Ejercicio8 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Para esta competencia, los valores son los siguientes: \n Piedra = 0 \n Papel = 1 \n Tijera = 2\n");
		System.out.println("Ingrese el valor del primer competidor");
		float Competidor1=scan.nextFloat();
		System.out.println("Ingrese el valor del segundo competidor");
		float Competidor2=scan.nextFloat();
		
		if (Competidor1 == Competidor2)
			System.out.println("Los Competidores Empataron");
		else
			if (Competidor1 == 0)
				if (Competidor2 == 1)
					System.out.println("El ganador es el segundo competidor, felicitaciones :D");
				else
					System.out.println("El ganador es el primer competidor, felicidades :D");
			else
				if (Competidor1 == 1)
					if (Competidor2 == 0)
						System.out.println("El ganador es el primer competidor, felicidades :D");
					else
						System.out.println("El ganador es el segundo competidor, felicitaciones :D");
				else
					if (Competidor1 == 2)
						if (Competidor2 == 0)
							System.out.println("El ganador es el segundo competidor, felicitaciones :D");
						else
							System.out.println("El ganador es el primer competidor, felicidades :D");
		scan=null;
	}
}
