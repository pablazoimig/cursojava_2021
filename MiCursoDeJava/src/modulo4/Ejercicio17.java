package modulo4;

import java.util.Scanner;

public class Ejercicio17
{
	public static void main(String[] args)
	{
		System.out.println("Ingrese el n�mero del que se quiera saber la tabla de multiplicar");
		Scanner scan = new Scanner(System.in);
		int numero = scan.nextInt();
		int sumapares = 0;
		
		System.out.println("La siguiente tabla corresponde al n�mero: " + numero);
		
		for(int i=1; i<11; i++ )
		{
			int resultado = numero * i;
			System.out.println(numero + "x" + i + "=" + resultado);
			if (resultado%2==0)
				sumapares = sumapares + resultado;
		}
		
		System.out.println("La suma de los n�meros pares es igual a " + sumapares);
		
		scan=null;
	}
}
