package modulo4;

import java.util.Scanner;

public class Ejercicio17b 
{
	public static void main(String[] args) 
	{
			System.out.println("Ingrese el n�mero del que se quiera saber la tabla de multiplicar");
			Scanner scan = new Scanner(System.in);
			int Numero = scan.nextInt();
			int SumadeImpares=0;
			int SumaTotal=0;
			
			System.out.println("La siguiente tabla corresponde al n�mero: " + Numero);
			
			for(int i=1; i<11; i++ )
			{
				int Resultado = Numero * i;
				System.out.println(Numero + "x" + i + "=" + Resultado);
				int EsPar = Resultado %2;
				SumaTotal = SumaTotal + Resultado;
				SumadeImpares = SumadeImpares + (Resultado * EsPar);
			}
			
			int SumadePares = SumaTotal - SumadeImpares;
			
			System.out.println("La suma de los n�meros pares es igual a " + SumadePares);
			
			scan=null;
	}
}
