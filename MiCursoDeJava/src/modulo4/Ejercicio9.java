package modulo4;

import java.util.Scanner;

public class Ejercicio9
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Para la competicion los valores son: \n Piedra = 0 \n Papel = 1 \n Tijera = 2\n");
		System.out.println("Ingrese el valor del primer competidor");
		float Competidor1=scan.nextFloat();
		System.out.println("Ingrese el valor del segundo competidor");
		float Competidor2=scan.nextFloat();
		
		if (Competidor1 == Competidor2)
			System.out.println("Los Competidores Empataron");
		else
		{
			if (Competidor1 == 0 && Competidor2 == 2)
				System.out.println("El ganador es el primer competidor, felicidades :D");
			else if (Competidor1 == 0 && Competidor2 == 1)
				System.out.println("El ganador es el segundo competidor, felicitaciones :D");
			else if (Competidor1 == 1 && Competidor2 == 0)
				System.out.println("El ganador es el primer competidor, felicidades :D");
			else if (Competidor1 == 1 && Competidor2 == 2)
				System.out.println("El ganador es el segundo competidor, felicitaciones :D");
			else if (Competidor1 == 2 && Competidor2 == 0)
				System.out.println("El ganador es el segundo competidor, felicitaciones :D");
			else if (Competidor1 == 2 && Competidor2 == 1)
				System.out.println("El ganador es el primer competidor, felicidades :D");
		}
		scan=null;
	}
}
