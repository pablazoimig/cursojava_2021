package modulo4;

import java.util.Scanner;

public class Ejercicio4 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una categoria: A, B o C");
		String categoria = scan.nextLine();
		
		if (categoria.equals("A"))
			System.out.println("Esta categoria corresponde a Hijos");
		else if (categoria.equals("B"))
			System.out.println("Esta categoria corresponde a Padres");
		else if (categoria.equals("C"))
			System.out.println("Esta categoria corresponde a Abuelos");
		else
			System.out.println("No ingres� una categoria v�lida");
		
		
		scan=null;
	}

}
