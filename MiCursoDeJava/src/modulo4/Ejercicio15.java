package modulo4;

import java.util.Scanner;

public class Ejercicio15
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la clase del auto (A, B o C)");
		char Tipo = scan.next().charAt(0);
		
		switch (Tipo)
		{
		case 'A':
			System.out.println("Esta clase de auto tiene 4 ruedas y un motor");
			break;
		case 'B':
			System.out.println("Esta clase de auto tiene 4 ruedas, un motor, cierre centralizado y aire");
			break;
		case 'C':
			System.out.println("Esta clase de auto tiene 4 ruedas, un motor, cierre centralizado, aire y airbag");
			break;
		case 'a':
			System.out.println("Esta clase de auto tiene 4 ruedas y un motor");
			break;
		case 'b':
			System.out.println("Esta clase de auto tiene 4 ruedas, un motor, cierre centralizado y aire");
			break;
		case 'c':
			System.out.println("Esta clase de auto tiene 4 ruedas, un motor, cierre centralizado, aire y airbag");
			break;
		default:
			System.out.println("La clase ingresada es inv�lida, pruebe de nuevo");
			break;
		}
		scan=null;
	}
}