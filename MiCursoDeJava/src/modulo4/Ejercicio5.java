package modulo4;

import java.util.Scanner;

public class Ejercicio5 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar el puesto en el torneo (Mediante un n�mero)");
		int Puesto =scan.nextInt();
		
		if(Puesto == 1)
			System.out.println("Obtuvo la medalla de oro, felicitaciones.");
		else if(Puesto == 2)
			System.out.println("Obtuvo la medalla de plata, estuvo muy cerca.");
		else if(Puesto == 3)
			System.out.println("Obtuvo la medalla de bronce, muy bien, suerte en la pr�xima.");
		else
			System.out.println("No obtuvo ninguna medalla, siga participando.");
		
		scan=null;
	}

}
