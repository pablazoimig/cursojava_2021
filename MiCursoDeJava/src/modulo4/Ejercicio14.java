package modulo4;

import java.util.Scanner;

public class Ejercicio14
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar el puesto en el torneo (Mediante un n�mero)");
		int Puesto = scan.nextInt();
		
		switch (Puesto)
		{
		case 1:
			System.out.println("Obtuvo la medalla de oro, felicitaciones.");
			break;
		case 2:
			System.out.println("Obtuvo la medalla de plata, estuvo muy cerca.");
			break;
		case 3:
			System.out.println("Obtuvo la medalla de bronce, muy bien, suerte en la pr�xima.");
			break;
		default:
			System.out.println("No obtuvo ninguna medalla, siga participando.");
			break;
		}
		scan=null;
	}
}