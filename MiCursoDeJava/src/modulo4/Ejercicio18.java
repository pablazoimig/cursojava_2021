package modulo4;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) 
	{
		System.out.println("Ingrese hasta qu� n�mero quiere conocer las tablas de multiplicar");
		Scanner scan = new Scanner(System.in);
		int numero = scan.nextInt();
		
		for(int n=0; n<numero+1; n++ )
		{
			System.out.println("Esta es la tabla del " + n);
			for(int i=1; i<11; i++ )
			{
				int resultado = n * i;
				System.out.println(n + "x" + i + "=" + resultado);
			}
			System.out.println("");
			
		}
	}

}
