package modulo4;

import java.util.Scanner;

public class Ejercicio12 
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un n�mero entero");
		int numero = scan.nextInt();
		
		if (numero > 0 || numero < 12)
			System.out.println("El n�mero ingresado pertenece a la primera docena");
		else if (numero > 12 || numero < 24)
			System.out.println("El n�mero ingresado pertenece a la segunda docena");
		else if (numero > 24 || numero < 36)
			System.out.println("El n�mero ingresado pertenece a la tercera docena");
		else
			System.out.println("El n�mero ingresado (" + numero + ") est� fuera de rango");
		
		scan=null;
	}
}
