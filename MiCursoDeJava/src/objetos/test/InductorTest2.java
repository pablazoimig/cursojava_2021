package objetos.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import objetos.Inductor;

public class InductorTest {
	//defino el lote de pruebas
	Inductor inductorVacio;
    Inductor inductorConDatos;
    List<Inductor> inductores;
    
	@Before
	public void setUp() throws Exception {
		//se ejecuta antes de cada testeo
		//creo los objetos
		inductorVacio 		= new Inductor()				 ;
		inductorConDatos 	= new Inductor("L1",0.003f, 3000);
		
		inductores = new ArrayList<>();		
		inductores.add(new Inductor("indu1", 0.1f, 100));
		inductores.add(new Inductor("indu2", 0.2f, 200));
		inductores.add(new Inductor("indu3", 0.3f, 300));
		inductores.add(new Inductor("indu4", 0.4f, 400));
		inductores.add(new Inductor("indu5", 0.5f, 500));
		inductores.add(new Inductor("indu6", 0.6f, 600));
		inductores.add(new Inductor("indu7", 0.7f, 700));
		inductores.add(new Inductor("indu8", 0.8f, 800));
		inductores.add(new Inductor("indu9", 0.9f, 900));
		inductores.add(new Inductor())					;
		
		
	}

	@After
	public void tearDown() throws Exception {
		//despues de cada testeo
		inductorVacio	=null;
		inductorConDatos=null;
		inductores		=null;
	}

	@Test
	public void testInductorNombre() {
		assertEquals("L por defecto", inductorVacio.getNombre());
	}
	//TODO alumnos, prueben el inductConDatosNombre
	@Test
	public void testInductoUnidad() {
		assertEquals("Hy", inductorVacio.getUnidad());
	}
	//TODO alumnos, prueben el inductConDatosUnidad
	@Test
	public void testInductorInductancia(){
		assertEquals(0.01f, inductorVacio.getValor(), 0.001);
	}
	//TODO alumnos, prueben el inductConDatosInductancia
	@Test
	public void testInductorFrecuencia() {
		assertEquals(1000, inductorVacio.getFrecuencia());
		
	}
	//TODO alumnos, prueben el inductConDatosFrecuencia
	@Test
	public void testCalcularImpedancia() {
		assertEquals(62.83f, inductorVacio.calcularImpedancia(), 0.01);
		
	}
	//TODO alumnos, prueben el inductConDatosImperanccia
	@Test
	public void testEqalsTRUE(){
		Inductor otro = new Inductor();
		assertTrue(inductorVacio.equals(otro));
	}
	//TODO alumnos, prueben el inductConDatosEquasTrue
	@Test
	public void testEqalsFALSE(){
		Inductor otro = new Inductor();
		otro.setNombre("otro");
		assertFalse(inductorVacio.equals(otro));
	}
	//TODO alumnos, prueben el inductConDatosEquasFalse
	@Test
	public void testEqualsEnListaTRUE(){
		assertTrue(inductores.contains(inductorVacio));
	}
	//TODO alumnos, prueben el inductConDatosListaEquasTrue
	@Test
	public void testEqualsEnListaFALSE(){
		assertFalse(inductores.contains(inductorConDatos));
	}
	//TODO alumnos, prueben el inductConDatosEquasFalse
}
