package modulo234_pantalla;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;

public class PantallaEjemploIF {

	private JFrame frame;
	private final JLabel lblNewLabel = new JLabel("Notas de materias");
	private JTextField textFieldNota1;
	private JTextField textFieldNota2;
	private JTextField textFieldNota3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaEjemploIF window = new PantallaEjemploIF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaEjemploIF() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 591, 532);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		lblNewLabel.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 28));
		lblNewLabel.setBounds(176, 31, 223, 33);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nota 1");
		lblNewLabel_1.setFont(new Font("Eras Medium ITC", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(29, 144, 48, 19);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Nota 2");
		lblNewLabel_1_1.setFont(new Font("Eras Medium ITC", Font.PLAIN, 16));
		lblNewLabel_1_1.setBounds(29, 191, 48, 19);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("Nota 3");
		lblNewLabel_1_2.setFont(new Font("Eras Medium ITC", Font.PLAIN, 16));
		lblNewLabel_1_2.setBounds(29, 239, 48, 19);
		frame.getContentPane().add(lblNewLabel_1_2);
		
		textFieldNota1 = new JTextField();
		textFieldNota1.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldNota1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textFieldNota1.setBounds(104, 145, 48, 20);
		frame.getContentPane().add(textFieldNota1);
		textFieldNota1.setColumns(10);
		
		textFieldNota2 = new JTextField();
		textFieldNota2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textFieldNota2.setColumns(10);
		textFieldNota2.setBounds(104, 192, 48, 20);
		frame.getContentPane().add(textFieldNota2);
		
		textFieldNota3 = new JTextField();
		textFieldNota3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textFieldNota3.setColumns(10);
		textFieldNota3.setBounds(104, 240, 48, 20);
		frame.getContentPane().add(textFieldNota3);
		
		JLabel lblPromedio = new JLabel("Promedio");
		lblPromedio.setFont(new Font("Eras Medium ITC", Font.PLAIN, 16));
		lblPromedio.setBounds(254, 165, 70, 19);
		frame.getContentPane().add(lblPromedio);
		
		JLabel lblPromedio_Resultado = new JLabel("");
		lblPromedio_Resultado.setBackground(Color.CYAN);
		lblPromedio_Resultado.setOpaque(true);
		lblPromedio_Resultado.setFont(new Font("Eras Medium ITC", Font.PLAIN, 16));
		lblPromedio_Resultado.setBounds(254, 195, 70, 19);
		frame.getContentPane().add(lblPromedio_Resultado);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(85, 306, 89, 29);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblImagen = new JLabel("");
		lblImagen.setIcon(new ImageIcon(PantallaEjemploIF.class.getResource("/iconos/uwu2.jpg")));
		lblImagen.setBounds(209, 239, 286, 227);
		frame.getContentPane().add(lblImagen);
	}
}
