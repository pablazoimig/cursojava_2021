package modulo234_pantalla;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JButton;

public class PantallaEjemploCiclo {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaEjemploCiclo window = new PantallaEjemploCiclo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaEjemploCiclo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 643, 651);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas de Multiplicar");
		lblNewLabel.setForeground(Color.MAGENTA);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(192, 23, 228, 31);
		frame.getContentPane().add(lblNewLabel);
		
		JComboBox cmbTabla = new JComboBox();
		cmbTabla.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmbTabla.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9"}));
		cmbTabla.setBounds(390, 98, 97, 22);
		frame.getContentPane().add(cmbTabla);
		
		JLabel lblNewLabel_1 = new JLabel("Tabla");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_1.setBounds(325, 95, 43, 22);
		frame.getContentPane().add(lblNewLabel_1);
		
		JList list = new JList();
		list.setFont(new Font("Tahoma", Font.PLAIN, 16));
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"2x0=0", "2x1=2", "2x2=4", "2x3=6", "2x4=8", "2x5=10", "2x6=12", "2x7=14", "2x8=16", "2x9=18"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setBounds(400, 141, 71, 225);
		frame.getContentPane().add(list);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.setBounds(120, 343, 89, 23);
		frame.getContentPane().add(btnNewButton);
	}
}
